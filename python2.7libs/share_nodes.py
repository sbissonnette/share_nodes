# -*- coding: utf-8 -*-
"""
Authors: Samuel Bissonnette,
"""

import os
import getpass
import hou


class ShareNodes(object):

    """
    This tool consist to copy nodes and send them to a specific user on network.
    """

    # Use Env Var or default path you can change
    SERVER_PATH = os.getenv('SHARE_NODES_SERVER_PATH') or '/tmp/houdini/share_nodes'

    def __init__(self):

        self.current_user = getpass.getuser()
        self.user_directory = self.create_user()
        self.user_list = self._retrieve_users()

    # Public ----------------------------------------------

    def create_user(self):
        """
        If not user directory exist, create it!

        :return: current user directory <str>
        """
        result = os.path.join(self.SERVER_PATH, self.current_user)
        if not os.path.exists(result):
            os.makedirs(result)
        return result

    def copy_nodes(self):
        """
        Prompt an UI with all available users and create cpio file for each.
        Selected Nodes needs to have all same parent!

        :return:
        """
        nodes = hou.selectedNodes()
        if not nodes:
            return
        # See if they are from the same parent
        parent = list()
        [parent.append(x.parent()) for x in nodes if not x.parent() in parent]
        if len(parent) > 1:
            raise Exception('')
        parent = parent[0]

        # Pop Users to send nodes
        users_index = hou.ui.selectFromList(self.user_list)

        # Loop in Users and create cpio
        for _index in users_index:
            cpio_path = os.path.join(self.SERVER_PATH, self.user_list[_index], 'nodes.cpio')
            if os.path.exists(cpio_path):
                os.remove(cpio_path)
            parent.saveChildrenToFile(nodes, [], cpio_path)

        return

    def paste_nodes(self):
        """
        Retrieve cpio file and load it in the NetworkEditor the user mouse over.

        Yeah since there are no other way to get current NetworkEditor...

        :return:
        """
        cpio_file = os.path.join(self.user_directory, 'nodes.cpio')
        if not os.path.exists(cpio_file):
            return
        pane = hou.ui.paneTabUnderCursor()
        if pane.type() != hou.paneTabType.NetworkEditor:
            raise Exception('Please mouse over the proper window!')
        parent = pane.pwd()
        parent.loadItemsFromFile(cpio_file)

        return

    # Non Public ------------------------------------------

    def _retrieve_users(self):
        """
        From SERVER_PATH, each subfolder are users with their given .cpio file

        :return: list of users [<str>,]
        """

        if not os.path.exists(self.SERVER_PATH):
            raise Exception('ERROR: {} does not exist! Contact your TD.'.format(self.SERVER_PATH))
        return [x for x in os.listdir(self.SERVER_PATH)]
