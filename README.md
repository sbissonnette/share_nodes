# Overview

From a User perspective: this tool is sending Houdini Nodes to another user's clipBoard

Under the hood: The User sending nodes is saving a .cpio file on a shared location of his selected nodes. 
The recipient then load this .cpio file in his current scene.

#### Supported Houdini Version
- 17.0.###
- 17.5.###
- 18.0.###
- 18.5.###
- 19.0.###

(It should work for 17.0+)

#### Supported OS

- Linux
- ~~MacOS~~ Not yet Tested
- ~~Windows~~ Not yet tested

# Usage

### Copy

![](.resources/copy_node.png)

Simply select nodes and hit `CTRL+SHIFT+C`. A UI should pop asking which user you want to share your node

**Selected nodes must be from same parent node!**

### Paste 
Hit `CTRL+SHIFT+V` to paste it! 

The User must be in the intended Context to paste the nodes.
(If Sender copied nodes from a "geo" context, recipient must be in a "geo" context as well!)

**VERY IMPORTANT: The user must Mouse Over his Network Editor in order to work.**

# Installation

## Shared install location
1. Choose a shared path location and copy the whole content of this tool in it.
2. Set `$HOUDINI_PATH` to the tool location
3. Set `$SHARE_NODES_SERVER_PATH` to a shared location path available for all Users. This is where the nodes 
will be shared across all users

## Local install

1. Simply copy `python2.7libs`, `toolbar` and `HotkeyOverrides` in you're `$HOUDINI_USER_PREF_DIR` folder.

2. Then you can either set `$SHARE_NODES_SERVER_PATH` Env Variable to set the shared location for copied nodes
OR go into `python2.7libs/share_nodes.py` file and edit this line:

```python
SERVER_PATH = os.getenv('SHARE_NODES_SERVER_PATH') or '/tmp/houdini/share_nodes'
```

![](.resources/install_locally.png)

# Limitations

- At the moment Network Box are not supported
- Every User must have a unique login name or else they are gonna share the same identity

# Troubleshooting

### Shortcut doesnt work

Make sure a Shelf called ShareNodes is available in your toolbar. Then validate the HotKey is set in the Network Pane

![](.resources/hotkey.png)

### I don't see any user when copying nodes

At the moment, user creation are triggered when they copy nodes for the first time. This ensure the installation
is working for every user. 

### Nothing is working!

- If installed locally: Delete the Houdini User Pref folder and repeat the installation process
- If installed on shared location: Delete the Houdini User Pref and retry
